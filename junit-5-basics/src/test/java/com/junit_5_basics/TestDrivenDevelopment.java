package com.junit_5_basics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Test Driven Development: Running MathUtils")
class TestDrivenDevelopment {

	@Test
	void testcomputeCircleArea() {
		MathUtils mathUtils = new MathUtils();	
		assertEquals(314.1592653589793, mathUtils.computeCircleArea(10),"Should return right circle area");
	}
	
}
